// BÀI 1: TÍNH TIỀN LƯƠNG NHÂN VIÊN

    // INPUT số ngày làm trong 1 tháng, ví dụ 26 ngày/tháng.
    var soNgayLam = 26;
    const tienCong =100000;

    var luongThang = null;

    luongThang = tienCong*soNgayLam;

    console.log('Lương tháng = ',luongThang.toLocaleString());

// BÀI 2: TÍNH GIÁ TRỊ TRUNG BÌNH

/*
    // INPUT 5 số bất kì, lấy ví dụ:
    var num1 = 5;
    var num2 = 10;
    var num3 = 15;
    var num4 = 20;
    var num5 = 25;

    var giaTriTrungBinh = null;

    giaTriTrungBinh = (num1+num2+num3+num4+num5)/5;

    console.log('Giá trị trung bình là: ',giaTriTrungBinh);
*/

// BÀI 3: QUY ĐỔI TIỀN

/*
    // INPUT số tiền USD muốn quy đổi sang VND, ví dụ 100 USD.
    var tienUSD = 100;
    const tyGia = 23500;

    var tienVND = null;

    tienVND = tienUSD*tyGia;

    console.log(tienUSD + ' USD = ' + tienVND.toLocaleString() + ' VND' );
*/

// BÀI 4: TÍNH DIỆN TÍCH, CHU VI HÌNH CHỮ NHẬT

/*
    // INPUT nhập vào giá trị bất kỳ của 2 cạnh hình chữ nhật, ví dụ a=10, b=20.
    var canhA = 10;
    var canhB = 20;

    var cVi = null;
    var dTich = null;

    cVi = (canhA+canhB)*2;
    dTich = canhA*canhB;

    console.log('Chu vi hình chữ nhật là: ' + cVi + '\n' + 'Diện tích hình chữ nhật là: ' + dTich);
*/

// BÀI 5: TÍNH TỔNG 2 KÍ SỐ

/*
    // INPUT nhập vào số có 2 chữ số bất kỳ, ví dụ 27.
    var soBatKy = 27;

    var soHangChuc = null;
    var soHangDonVi = null;
    var tongHaiKiSo = null;

    soHangChuc = Math.floor(soBatKy/10);
    soHangDonVi = soBatKy%10;

    tongHaiKiSo = soHangChuc + soHangDonVi;


    console.log('Tổng hai kí số đã nhập là: ',tongHaiKiSo);
*/

